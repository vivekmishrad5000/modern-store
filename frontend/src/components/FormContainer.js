import { Flex, flex } from '@chakra-ui/react';

const FormcContainer = ({children, width = 'xl' }) => {
    return (
        <Flex direction='column' boxShadow='md' rounded='md' bgColor='white' p='10' width={width}>
            {children}
        </Flex>
    );
};

export default FormcContainer